import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { DatosRoutingModule } from './datos/datos-routing.module';



@NgModule({
  declarations: [],
  imports: [
    NativeScriptCommonModule,
    DatosRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MiModuloModule { }
