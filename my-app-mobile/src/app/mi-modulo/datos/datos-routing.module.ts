import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { DatosComponent } from "./datos.component";
import { Datos1Component } from "./datos1/datos1.component";
import { Datos2Component } from "./datos2/datos2.component";

const routes: Routes = [
    { path: "", component: DatosComponent },
    { path: "datos1", component: Datos1Component },
    { path: "datos2", component: Datos2Component }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DatosRoutingModule { }
