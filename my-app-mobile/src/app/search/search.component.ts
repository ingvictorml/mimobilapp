import * as Toast from 'nativescript-toast';
import * as app from "tns-core-modules/application";
import { AppState } from "../app.module";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    moduleId: module.id,

})
export class SearchComponent implements OnInit {
    resultados: Array<string> = [];

    @ViewChild("layout", null) layout: ElementRef;

    constructor(private noticias: NoticiasService,
        private store: Store<AppState>) {
        // Use the component constructor to inject providers.

    }

    ngOnInit(): void {
        // Init your component properties here.
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    //Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.resultados.push("xxxxxxx");
            pullRefresh.refreshing = false;
        }, 2000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new 
            Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string){
        console.dir("buscarAhora"+ s);
        this.noticias.buscar(s).then(( r : any )=> {
            console.log("resultados buscarAhora "+ JSON.stringify(r));
            this.resultados = r;

        }, (e)=> {
            console.log("error buscarAhora "+e);
            var Toast = Toast.makeText("Error en la busqueda", Toast.duration.SHORT);
            Toast.show();

        })

        /* const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150

        }).then(()=>layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }))*/
    }
}
