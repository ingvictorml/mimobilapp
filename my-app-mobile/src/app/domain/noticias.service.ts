import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
import { Noticia } from "../models/noticias-model";

const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {
  //private noticias: Array<string> = [];
  api: string = "https://57d59507a52a.ngrok.io"; // EndPoint de la api

  constructor() {
    this.getDB((db) => {
      console.dir(db);
      db.each("select * from logs",
        (err, fila) => console.log("Fila: ", fila),
        (err, totales) => console.log("Filas totales", totales)
      );
    }, () => console.log("Error on getDB"));
  }

  getDB(fnOk, fnError) {
    return new sqlite("my_db_logs2", (err, db) => {
      if (err) {
        console.error("Error al abrir DB!", err);
      } else {
        console.log("BD abierta: ", db.isOpen() ? "Si" : "No");
        db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
          .then((id) => {
            console.log("CREATE TABLE OK");
            fnOk(db);
          }, (error) => {
            console.log("CREATE TABLE ERROR", error);
            fnError(error);
          });
      }
    });
  }

  agregar(s: string) {
    return request({
      url: this.api + "/favs",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
        nuevo: s
      })
    });
  }

  buscar(s: string) {
    this.getDB((db) => {
      db.execSQL("INSERT INTO logs (texto) VALUES (?)", [s],
        (err, id) => console.log("Nuevo id: ", id)
      );
    }, () => console.log("Error on buscarDB"));

    return getJSON(this.api + "/get?q=" + s)
  }


  favs() {
    return getJSON(this.api + "/favs");
  }

  postFavorita(n: Noticia) {
    return request({
      url: this.api + "/favs",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
        nuevo: n
      })
    })
  }

  deleteFavorita(n: Noticia) {
    return request({
      url: this.api + "/favs",
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
        favorita: n
      })
    })
  }
}
